# Vuejs
## Instrucciones de uso
Ejecutar el docker-compose
```
$ docker-compose up
```
## Rutas 
### Vista frontend
* [http://localhost:8084](http://localhost:8084)
### API backend
* [http://localhost:3084](http://localhost:3084)
